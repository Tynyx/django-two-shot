from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Receipt, ExpenseCategory, Account
from django.forms import ModelForm
# Create your views here.


@login_required
def receipt_List(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'categories': categories,
    }
    return render(request, 'receipts/category.html', context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        'accounts': accounts,
    }
    return render(request, 'receipts/accounts_list.html', context)


class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ['name']


@login_required
def create_category(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            Expense_Category = form.save(commit=False)
            Expense_Category.owner = request.user
            Expense_Category.save()
            return redirect('/receipts/categories/')
    else:
        form = CategoryForm()
    return render(request, 'receipts/create_category.html', {'form': form})


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'number']


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('/receipts/accounts/')
    else:
        form = AccountForm()
        context = {
            'form': form,
        }
        return render(request, 'receipts/create_account.html', context)